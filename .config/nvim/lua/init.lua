-- vim:fdm=marker:

require('paq'){
    "savq/paq-nvim";

    {"nvim-treesitter/nvim-treesitter", run = function() vim.cmd 'TSUpdate' end};
    "nvim-treesitter/nvim-treesitter-textobjects";
    "nvim-treesitter/nvim-treesitter-refactor";

    "nvim-telescope/telescope.nvim";
    {"nvim-telescope/telescope-fzf-native.nvim", run = "make"};
    "nvim-lua/plenary.nvim";

    "machakann/vim-sandwich";
    "windwp/nvim-autopairs";
    "numToStr/Comment.nvim";

    "norcalli/nvim-colorizer.lua";
    "lukas-reineke/indent-blankline.nvim";
    "psliwka/vim-smoothie";
    "lewis6991/gitsigns.nvim";
    "nvim-lualine/lualine.nvim";
    "RRethy/nvim-base16";
}

-- require('impatient')


-- config
-- require('range-highlight').setup()

-- require("which-key").setup()

-- Main
require('base16-colorscheme').setup({
    base00 = '#181818', base01 = '#282a2e', base02 = '#373b41', base03 = '#969896',
    base04 = '#b4b7b4', base05 = '#c5c8c6', base06 = '#e0e0e0', base07 = '#ffffff',
    base08 = '#cc342b', base09 = '#f96a38', base0A = '#fba922', base0B = '#198844',
    base0C = '#3971ed', base0D = '#3971ed', base0E = '#a36ac7', base0F = '#3971ed'
})

require('colorizer').setup()

require('nvim-autopairs').setup{check_ts = true}

require('gitsigns').setup{numhl = false}

require('indent_blankline').setup{
    char_list = {'|', '¦', '┆', '┊'},
    show_first_indent_level = false,
    max_indent_increase = 1,
}

require('Comment').setup()

require('telescope').load_extension('fzf')

-- {{{ lualine
require('lualine').setup{
  options = {
    theme = 'base16'
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {{'filename', path = 1}},
    lualine_c = {'branch', 'diff'},
    lualine_x = {},
    lualine_y = {'filetype', 'progress'},
    lualine_z = {'location'}
  }
}
-- }}}

-- {{{ Telescope
require('nvim-treesitter.configs').setup {
    ensure_installed = "all",
    context_commentstring = {
        enable = true
    },
    autopairs = {
        enable = true
    },
    highlight = {
        enable = true,
        -- use_languagetree = false,
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "gnn",
            node_incremental = "grn",
            scope_incremental = "grc",
            node_decremental = "grm",
        },
    },
    indent = {
        enable = false
    },
    refactor = {
        navigation = {
            enable = true,
            keymaps = {
                goto_definition = "gnd",
                list_definitions = "gnD",
                list_definitions_toc = "gO",
                goto_next_usage = "<a-*>",
                goto_previous_usage = "<a-#>",
            },
        },
        smart_rename = {
            enable = true,
            keymaps = {
                smart_rename = "grr",
            },
        },
        highlight_current_scope = { enable = false },
        highlight_definitions = { enable = false },
    },
    textobjects = {
        select = {
            enable = true,
            keymaps = {
                ["af"] = "@function.outer",
                ["if"] = "@function.inner",
                ["ac"] = "@class.outer",
                ["ic"] = "@class.inner",
                ["aa"] = "@parameter.outer", -- an /a/rgument
                ["ia"] = "@parameter.inner",
            },
        },
        swap = {
            enable = true,
            swap_next = {
                ["<leader>a"] = "@parameter.inner",
            },
            swap_previous = {
                ["<leader>A"] = "@parameter.inner",
            },
        },
    },
}
--- }}}

