" vim:fdm=marker:
" {{{ BASIC SETUP
set number relativenumber " enable line numbers
set splitbelow splitright " sane splitting
set expandtab tabstop=4 softtabstop=4 shiftwidth=4 " fix the way tabs work
set noshowmode
set smartcase ignorecase
set undofile
set hidden " switch buffers without saving
set inccommand=nosplit
set virtualedit=block
set termguicolors
set foldcolumn=1
set scrolloff=1
set timeoutlen=500
set mouse+=a " enable mouse support
set clipboard=unnamedplus " copy/paste from * reg
" set clipboard=unnamed " copy/paste from + reg
" let g:python3_host_prog = '/usr/bin/python'

" new lua filetype detection (remove in 0.8)
let g:do_filetype_lua = 1
let g:did_load_filetypes = 0

" netwr stuff
let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 15
let g:netrw_home = '~/.local/share/netrw/'
nmap <leader>d :Vex<CR>
" }}}

" MAPS
let mapleader = " " " leader is space
command! RT !ctags -R .
au TextYankPost * silent! lua vim.highlight.on_yank{timeout=250}
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fd <cmd>lua require('telescope.builtin').find_files{find_command = {'dot', 'getfiles'}}<cr>
nmap <leader>o o<Esc>
nmap <leader>O O<Esc>
nmap <silent> <leader>l <cmd>noh<cr>

" ABBREVIATIONS
iab flase false
iab Flase False
iab slef self

" SYNTAX
syntax keyword Todo contained TODO FIXME XXX
" match Error / \+$/
" set listchars=eol:↲,trail:.,extends:>,precedes:<

" LUA STUFF
lua require('init')
