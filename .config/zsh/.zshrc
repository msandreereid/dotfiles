# vim:fdm=marker:
# {{{ P10K INSTANT PROMPT
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi # }}}

# {{{ SETTINGS
bindkey -e # vim/emacs keys
setopt auto_cd
# alias tmux='tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf' # move tmux config to XDG
setopt extendedglob
# {{{ HISTORY OPTIONS
setopt auto_pushd
setopt pushd_ignore_dups
setopt extended_history
setopt share_history
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_save_no_dups
setopt hist_verify
HISTFILE="$XDG_DATA_HOME/zsh/history"
HISTSIZE=10000
SAVEHIST=10000
# fishtory
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
zle -N set-local-history
bindkey '^K' up-line-or-beginning-search
bindkey '^J' down-line-or-beginning-search
bindkey '^[[A' up-line-or-beginning-search
bindkey '^[[B' down-line-or-beginning-search
bindkey '^H' set-local-history
# }}}
# extra keybinds
# bindkey '^[[1;5C' forward-word
bindkey '^[[3~' delete-char # map del key properly
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^v' edit-command-line
# completion
autoload -Uz compinit
compinit -d $ZSH_COMPDUMP
# setopt COMPLETE_ALIASES
alias sudo='sudo ' # sudo use aliases
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' # case insensitive complete
# }}}

# {{{ COLOUR STUFF
autoload -U colors && colors
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
# export LS_COLORS='di=1;34:ln=35:so=32:pi=33:ex=31:bd=1;36:cd=1;33:su=30;41:sg=30;46:tw=30;42:ow=30;43'
alias ls='ls --color=auto -h --group-directories-first'
source "$XDG_DATA_HOME"/zsh/lscolors.sh
alias diff='diff --color=auto'
alias ip='ip -color=auto'
# {{{ LESS COLOURS
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'
export LESS_TERMCAP_md=$'\E[1;36m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_us=$'\E[1;32m'
export LESS_TERMCAP_ue=$'\E[0m'
# }}}
# {{{ fzf colours
_gen_fzf_default_opts() {
local color00='#181818'
local color01='#282a2e'
local color02='#373b41'
local color03='#969896'
local color04='#b4b7b4'
local color05='#c5c8c6'
local color06='#e0e0e0'
local color07='#ffffff'
local color08='#CC342B'
local color09='#F96A38'
local color0A='#FBA922'
local color0B='#198844'
local color0C='#3971ED'
local color0D='#3971ED'
local color0E='#A36AC7'
local color0F='#3971ED'
export FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS"\
" --color=bg+:$color01,bg:$color00,spinner:$color0C,hl:$color0D"\
" --color=fg:$color04,header:$color0D,info:$color0A,pointer:$color0C"\
" --color=marker:$color0C,fg+:$color06,prompt:$color0A,hl+:$color0D"
}
_gen_fzf_default_opts # }}}
zmodload zsh/zpty
pty() {
    zpty pty-${UID} ${1+$@}
    if [[ ! -t 1 ]];then
        setopt local_traps
        trap '' INT
    fi
    zpty -r pty-${UID}
    zpty -d pty-${UID}
}
ptyless() {
    pty $@ | less
}
# }}}

# ALIASES
alias td="tmux detach"
alias ta="tmux attach"
alias v="nvim"
# alias grep="rg"
alias sudoe="sudoedit"
alias reconnect="iwctl station wlan0 disconnect && iwctl station wlan0 connect 'Optify'"
alias sudod="SUDO_EDITOR='$DIFFPROG' sudoedit"
alias pacdiff="SUDO_EDITOR='$DIFFPROG' DIFFPROG=sudoedit pacdiff"
alias least="/usr/share/nvim/runtime/macros/less.sh"
alias yay="pikaur"
alias cleanpkg="pikaur -Rsu \$(pikaur -Qttdq)"
alias rcleanpkg="pikaur -Rsu \$(comm -23 <(pikaur -Qq | sort) <(pacman -Qeq | sort))"
alias resource="source $HOME/.zshenv && source $XDG_CONFIG_HOME/zsh/.zshrc"
alias docker="sudo docker"
alias docker-compose="sudo docker-compose"
alias ll="ls -Al"
alias vrc="$VISUAL $XDG_CONFIG_HOME/nvim/init.vim"
alias zrc="$VISUAL $XDG_CONFIG_HOME/zsh/.zshrc"
# alias dot="git --git-dir=$XDG_DATA_HOME/dotbare/ --work-tree=$HOME "
alias edot="dot edit"
alias adot="dot upd"
alias dpm="dot plug"
alias db="dotbare"
alias java8="/usr/lib/jvm/java-8-openjdk/jre/bin/java"

# FUNCTIONS
pkgtop() {
    pikaur $@\q | $FUZZY --preview 'pikaur -Qil {}' --layout=reverse --bind 'enter:execute(pikaur -Qil {} | less)'
}
cht() {
    curl cheat.sh/$1
}
ldplug() {
    zsh-defer source $ZP/${1}/${1}.plugin.zsh
}

compdef _git dot

# PLUGINS
source $ZP/zsh-defer/zsh-defer.plugin.zsh
source $ZP/powerlevel10k/powerlevel10k.zsh-theme
ldplug dotbare
ldplug fzf-tab
ldplug fast-syntax-highlighting
ldplug zsh-autosuggestions
zsh-defer source /usr/share/fzf/key-bindings.zsh

# p10k config
source "$XDG_CONFIG_HOME"/zsh/p10k.zsh
