#!/bin/bash

STATUS=$(playerctl status)
if [[ $STATUS != "Playing" ]] && [[ $STATUS != "Paused" ]]; then
    exit
fi

CURRENT=$(printf "%.0f" $(playerctl position))
LENGTH=$(playerctl metadata --format "{{ mpris:length }}")

PERCENTAGE=$(($CURRENT*30/($LENGTH/1000000)))

printf '❰╺'

for ((i = 0 ; i < $PERCENTAGE ; i++)); do
  printf '─'
done

if [[ $STATUS == "Playing" ]]; then
    printf '╼▶'
elif [[ $PERCENTAGE -eq 30 ]]; then
    printf '─╸'
else
    printf '╼⏹'
fi

for ((i = 0 ; i < $((30 - $PERCENTAGE)) ; i++)); do
    printf ' '
done

printf '❱'

