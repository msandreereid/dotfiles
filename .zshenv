# XDG BASE DIR VARS
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_DATA_HOME="$HOME"/.local/share
export XDG_CACHE_HOME="$HOME"/.cache
export XDG_STATE_HOME="$HOME"/.local/state

export ZDOTDIR="$XDG_CONFIG_HOME"/zsh
export ZSH_COMPDUMP="$XDG_CACHE_HOME"/zsh/zcompdump-"$USER"-"$ZSH_VERSION"
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export PYTHONSTARTUP="$XDG_CONFIG_HOME"/pythonrc
# export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java # doesnt even matter as fonts still creates folder

# export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
# export XAUTHORITY="$XDG_DATA_HOME"/Xauthority

export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter

export VISUAL="nvim"
export EDITOR="nvim"
export DIFFPROG="nvim -d"
export FUZZY="fzf-tmux"
# export FUZZY="sk"
export ZP="$XDG_DATA_HOME"/zsh/
export FZF_DEFAULT_COMMAND="fd"
export FZF_CTRL_T_COMMAND="fd"
export FZF_ALT_C_COMMAND="fd --type d"
export DOTBARE_DIR="$HOME/.local/share/dotbare"
path=(~/.local/bin/ $path)
fpath=($fpath $XDG_DATA_HOME/zsh/completions)
